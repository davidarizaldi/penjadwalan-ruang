<?php
require('db.php');
date_default_timezone_set('Asia/Kuala_Lumpur');
$results_per_page = 10;
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title>Tabel Manage Ruang</title>
</head>

<body>
  <div>
    <form method="get">
      <div class="row">
        <div class="col-3">
          <div class="form-group">
            <label>Kode Ruang</label>
            <input type="text" name="kd_ruang" id="kd_ruang" class="form-control border" value="<?php echo isset($_GET['kd_ruang'])?$_GET['kd_ruang']:'' ?>" placeholder="A3-02">
          </div>
        </div>
        <div class="col-6">
          <div class="form-group">
            <label>Deskripsi Ruang</label>
            <input type="text" name="deskripsi_ruang" id="deskripsi_ruang" class="form-control border" value="<?php echo isset($_GET['deskripsi_ruang'])?$_GET['deskripsi_ruang']:'' ?>" placeholder="Gedung A Lantai 3 Sebelah Kanan">
          </div>
        </div>
        <div class="col-3">
          <div class="form-group">
            <label></label>
            <div class="row justify-content-between m-1 mt-2">
              <button type="submit" name="submit" id="submit" value="search" class="btn btn-primary col-3"><span data-feather="search"></span></button>
              <a href="<?php echo $_SERVER['PHP_SELF']; ?>" class="btn btn-danger col-3"><span data-feather="x"></span></a>
              <a type="button" class="btn btn-success col-4"  href="crud_ruang.php?jenis=insert">Insert</a>
            </div>
          </div>
        </div>
      </div>
    </form>
  </div>
  <div class="table-responsive">
    <table class="table table-striped table-hover">
      <thead>
        <tr>
          <th scope="col">#</th>
          <th scope="col">Kode Ruang</th>
          <th scope="col">Deskripsi Ruang</th>
          <th scope="col">Actions</th>
        </tr>
      </thead>
      <tbody>
        <?php
        $filter = " WHERE TRUE ";
        
        if(isset($_GET['kd_ruang']) && $_GET['kd_ruang'] != '')
          $filter = $filter."AND (kd_ruang LIKE '%".$_GET["kd_ruang"]."%') ";
        if(isset($_GET['deskripsi_ruang']) && $_GET['deskripsi_ruang'] != '')
          $filter = $filter."AND (deskripsi_ruang LIKE '%".$_GET["deskripsi_ruang"]."%') ";
        
        if (isset($_GET["page"])) $page = $_GET["page"];
        else $page = 1;
        $start_from = ($page-1) * $results_per_page;
        
        $query = "SELECT * FROM ruang $filter ORDER BY kd_ruang LIMIT $start_from, $results_per_page;";
        $result = $conn->query($query);
        $num = 1;
        while($row = mysqli_fetch_assoc($result) ) {
        ?>
        <tr>
          <td scope="row"><?php echo $num; ?></td>
          <td><?php echo $row["kd_ruang"]; ?></td>
          <td><?php echo $row["deskripsi_ruang"]; ?></td>
          <td>
            <ul class="list-inline m-0">
              <li class="list-inline-item">
                <a href="crud_ruang.php?jenis=edit&kd_ruang_sekarang=<?php echo $row["kd_ruang"]; ?>" class="btn btn-outline-primary btn-sm"><span data-feather="edit"></a>
              </li>
              <li class="list-inline-item">
                <a href="crud_ruang.php?jenis=delete&kd_ruang_sekarang=<?php echo $row["kd_ruang"]; ?>" class="btn btn-outline-danger btn-sm"><span data-feather="trash-2"></a>
              </li>
            </ul>
          </td>
        </tr>
        <?php $num++; } ?>
      </tbody>
    </table>
  </div>
  <nav aria-label="Page Navigation">
    <ul class="pagination justify-content-center">
      <?php
      $sql = "SELECT COUNT(*) AS total FROM ruang ".$filter.";"; 
      $result = $conn->query($sql);
      $row = $result->fetch_assoc();
      $total_pages = ceil($row["total"] / $results_per_page);
      if($total_pages == 0) $total_pages = 1;
      $url=$_SERVER["QUERY_STRING"];
      if(substr($url, 0, 4)=="page") {
        $url = substr($url, strpos($url, "&")+1);
      }
      ?>
      <li class="page-item<?php echo ($page == 1?' disabled':''); ?>">
        <a class="page-link" href="<?php echo $_SERVER["PHP_SELF"]."?page=".($page-1)."&".$url; ?>" aria-label="Previous">
          <span aria-hidden="true">&laquo;</span>
          <span class="sr-only">Previous</span>
        </a>
      </li>
      <?php
      for ($i=1; $i<=$total_pages; $i++) {
        echo "
          <li class='page-item".($page == $i?' active':'')."'>
            <a class='page-link' href='".$_SERVER["PHP_SELF"]."?page=".$i."&".$url."'>$i</a>
          </li>
        ";
      }
      ?>
      <li class="page-item<?php echo ($page == $total_pages?' disabled':''); ?>">
        <a class="page-link" href="<?php echo $_SERVER["PHP_SELF"]."?page=".($page+1)."&".$url; ?>" aria-label="Next">
          <span aria-hidden="true">&raquo;</span>
          <span class="sr-only">Next</span>
        </a>
      </li>
    </ul>
  </nav>
</body>
</html>
