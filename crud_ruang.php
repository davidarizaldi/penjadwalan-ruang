<?php
require('db.php');
$status = "";
if($_REQUEST['jenis'] == "delete") {
  $check_query = "SELECT * from jadwal WHERE kd_ruang = '".$_REQUEST['kd_ruang_sekarang']."';";
  $result = $conn->query($check_query);
  $row = mysqli_fetch_assoc($result);
  if(!empty($row));
  else {
    $query = "DELETE FROM ruang WHERE kd_ruang = '".$_REQUEST['kd_ruang_sekarang']."';";
    $result = mysqli_query($conn,$query) or die ( mysqli_error());
  }
  header("Location: manage_ruang.php");
} else if($_REQUEST['jenis'] == "insert" AND count($_POST) > 0) {
  $ins_query = "INSERT INTO ruang(kd_ruang,deskripsi_ruang)
                VALUES(
                  '".$_POST['kd_ruang']."',
                  '".$_POST['deskripsi_ruang']."'
                )";
  $result = $conn->query($ins_query);
  if($result) $status = "Ruang berhasil dimasukkan.";
  else $status = "Ruang gagal dimasukkan.";
} else if($_REQUEST['jenis'] == "edit") {
  if(count($_POST) > 0) {
    $edit_query = "UPDATE ruang
                  SET
                    kd_ruang = '".$_POST['kd_ruang']."',
                    deskripsi_ruang = '".$_POST['deskripsi_ruang']."'
                  WHERE kd_ruang = '".$_REQUEST['kd_ruang_sekarang']."'";
    $result = $conn->query($edit_query);
    if($result) $status = "Ruang berhasil diedit.";
    else $status = "Ruang gagal diedit.";
  }
  $query = "SELECT * FROM ruang WHERE kd_ruang='".$_REQUEST['kd_ruang_sekarang']."';";
  $result = mysqli_query($conn, $query) or die ( mysqli_error());
  $row = mysqli_fetch_assoc($result);
}
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="David Arizaldi Muhammad">
    <title><?php echo ucwords($_REQUEST['jenis']); ?> Ruang</title>

    <link href="dist/css/bootstrap.min.css" rel="stylesheet">
    <style>
      .bd-placeholder-img {
        font-size: 1.125rem;
        text-anchor: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
      }

      @media (min-width: 768px) {
        .bd-placeholder-img-lg {
          font-size: 3.5rem;
        }
      }
    </style>
    <link href="nav.css" rel="stylesheet">
  </head>
  <body>
    
<nav class="navbar navbar-dark sticky-top bg-dark flex-md-nowrap p-0 shadow">
  <a class="navbar-brand col-md-3 col-lg-2 mr-0 px-3" href="#">FKIP UNRAM</a>
  <button class="navbar-toggler position-absolute d-md-none collapsed" type="button" data-toggle="collapse" data-target="#sidebarMenu" aria-controls="sidebarMenu" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <label class="form-control form-control-dark w-100" type="text"><?php echo strtoupper($_REQUEST['jenis']); ?> RUANG</label>
</nav>

<div class="container-fluid">
  <div class="row">
    <nav id="sidebarMenu" class="col-md-3 col-lg-2 d-md-block bg-light sidebar collapse">
      <div class="sidebar-sticky pt-3">
        <ul class="nav flex-column">
          <li class="nav-item">
            <a class="nav-link" href="index.php">
              <span data-feather="home"></span>
              Ruang Ujian
            </a>
          </li>
        </ul>

        <h6 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted">
          <span>ADMIN</span>
        </h6>
        <ul class="nav flex-column mb-2">
          <li class="nav-item">
            <a class="nav-link" href="manage_jadwal.php">
              <span data-feather="calendar"></span>
              Manage Jadwal
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="manage_jadwal_ujian.php">
              <span data-feather="calendar"></span>
              Manage Jadwal Ujian
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link active" href="manage_ruang.php">
              <span data-feather="box"></span>
              Manage Ruang <span class="sr-only">(current)</span>
            </a>
          </li>
					<li class="nav-item">
            <a class="nav-link" href="export.php">
              <span data-feather="printer"></span>
              Export
            </a>
          </li>
        </ul>
      </div>
    </nav>

    <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-md-4">
      <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-1 pb-2 mb-3">
      </div>
      <div>
        <form name="manage_jadwal_form" id="manage_jadwal_form" method="post" action="">
          <input type="hidden" name="jenis" id="jenis" value="<?php echo $_REQUEST['jenis']; ?>">
          <input type="hidden" name="kd_ruang_sekarang" id="kd_ruang_sekarang" value="<?php echo isset($_REQUEST['kd_ruang_sekarang'])?$_REQUEST['kd_ruang_sekarang']:''; ?>">
          <div class="form-row">
            <div class="form-group col-md-6">
              <label for="kd_ruang">Kode Ruang</label>
              <input type="text" class="form-control border" name="kd_ruang" id="kd_ruang"<?php echo ($_REQUEST['jenis']=="edit"?' readonly':''); ?> required value="<?php echo isset($row['kd_ruang'])?$row['kd_ruang']:''; ?>">
            </div>
          </div>
          <div class="form-row">
            <div class="form-group col-md-6">
              <label for="deskripsi_ruang">Deskripsi Ruang</label>
              <input type="text" class="form-control border" name="deskripsi_ruang" id="deskripsi_ruang" value="<?php echo isset($row['deskripsi_ruang'])?$row['deskripsi_ruang']:''; ?>">
            </div>
          </div>
          <p class="text-success text-center"><?php if($status!="") { echo $status; } ?></p>
          <button type="submit" class="btn btn-primary">Submit</button>
        </form>
      </div>
    </main>
  </div>
</div>
    
    <script src="dist/js/feather.min.js"></script>
    <script src="nav.js"></script>
    <script src="dist/js/jquery.min.js"></script>
    <script src="dist/js/bootstrap.min.js"></script>
  </body>
</html>
