<?php
require('db.php');
date_default_timezone_set('Asia/Kuala_Lumpur');
if($_GET["jenis"] == "jadwal") {
	header('Content-Type: text/csv; charset=utf-8');
	header('Content-Disposition: attachment; filename=Jadwal Khusus '.date("Y-m-d H:i:s").'.csv');
	$output = fopen("php://output", "w");
	fputcsv($output, array('id_jadwal', 'tanggal', 'waktu_mulai', 'waktu_akhir', 'kegiatan', 'peminjam', 'kd_ruang', 'nim_mahasiswa', 'nama_mahasiswa', 'judul_skripsi', 'kd_dosen_pembimbing', 'kd_dosen_penguji'));
	$query = "SELECT * FROM jadwal ORDER BY tanggal, waktu_mulai";
	$result = mysqli_query($conn, $query);
	while($row = mysqli_fetch_assoc($result)) {
		fputcsv($output, $row);
	}
	fclose($output);
} else if($_GET["jenis"] == "ruang") {
	header('Content-Type: text/csv; charset=utf-8');
	header('Content-Disposition: attachment; filename=Ruang Khusus '.date("Y-m-d H:i:s").'.csv');
	$output = fopen("php://output", "w");
	fputcsv($output, array('kd_ruang', 'deskripsi_ruang'));
	$query = "SELECT * FROM ruang ORDER BY kd_ruang";
	$result = mysqli_query($conn, $query);
	while($row = mysqli_fetch_assoc($result)) {
		fputcsv($output, $row);
	}
	fclose($output);
}
?>