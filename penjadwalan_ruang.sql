-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 30, 2021 at 08:21 AM
-- Server version: 10.4.19-MariaDB
-- PHP Version: 8.0.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `penjadwalan_ruang`
--

-- --------------------------------------------------------

--
-- Table structure for table `jadwal`
--

CREATE TABLE `jadwal` (
  `id_jadwal` int(10) NOT NULL,
  `tanggal` date NOT NULL,
  `waktu_mulai` time NOT NULL,
  `waktu_akhir` time NOT NULL,
  `kegiatan` varchar(20) NOT NULL,
  `peminjam` varchar(50) NOT NULL,
  `kd_ruang` varchar(10) NOT NULL,
  `nim_mahasiswa` varchar(9) DEFAULT NULL,
  `nama_mahasiswa` varchar(50) DEFAULT NULL,
  `judul_skripsi` varchar(50) DEFAULT NULL,
  `kd_dosen_pembimbing` varchar(20) DEFAULT NULL,
  `kd_dosen_penguji` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `jadwal`
--

INSERT INTO `jadwal` (`id_jadwal`, `tanggal`, `waktu_mulai`, `waktu_akhir`, `kegiatan`, `peminjam`, `kd_ruang`, `nim_mahasiswa`, `nama_mahasiswa`, `judul_skripsi`, `kd_dosen_pembimbing`, `kd_dosen_penguji`) VALUES
(33, '2021-06-03', '16:00:00', '18:00:00', 'Rapat', 'Himpunan Mahasiswa Fisika', 'A3-01', NULL, NULL, NULL, NULL, NULL),
(34, '2021-05-27', '13:00:00', '15:30:00', 'Rapat', 'Himpunan Mahasiswa Matematika', 'A3-02', NULL, NULL, NULL, NULL, NULL),
(35, '2021-05-31', '08:00:00', '10:00:00', 'Proposal', 'Adam Fikri', 'C2-04', 'F1D018001', 'Adam Fikri', 'Aplikasi Game', 'MUN', 'MUN'),
(36, '2021-05-31', '12:00:00', '15:00:00', 'Skripsi', 'David Arizaldi Muhammad', 'C2-04', 'F1D018011', 'David Arizaldi Muhammad', 'Aplikasi Penjadwalan', 'MUN', 'UMI'),
(37, '2021-06-01', '15:30:00', '17:00:00', 'Skripsi', 'Hendy Marcellino', 'C2-04', 'F1D018021', 'Hendy Marcellino', 'Aplikasi Penjadwalan Game', 'UMI', 'MUN,AMI'),
(38, '2021-05-26', '08:00:00', '10:00:00', 'Praktek', 'MUN', 'A3-01', NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `ruang`
--

CREATE TABLE `ruang` (
  `kd_ruang` varchar(10) NOT NULL,
  `deskripsi_ruang` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `ruang`
--

INSERT INTO `ruang` (`kd_ruang`, `deskripsi_ruang`) VALUES
('A3-01', 'Gedung A Lantai 3 Nomor 1'),
('A3-02', 'Gedung A Lantai 3 Nomor 2'),
('C2-04', 'Gedung C Lantai 2 Nomor 4');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `jadwal`
--
ALTER TABLE `jadwal`
  ADD PRIMARY KEY (`id_jadwal`),
  ADD KEY `fk_kd_ruang` (`kd_ruang`);

--
-- Indexes for table `ruang`
--
ALTER TABLE `ruang`
  ADD PRIMARY KEY (`kd_ruang`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `jadwal`
--
ALTER TABLE `jadwal`
  MODIFY `id_jadwal` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `jadwal`
--
ALTER TABLE `jadwal`
  ADD CONSTRAINT `fk_kd_ruang` FOREIGN KEY (`kd_ruang`) REFERENCES `ruang` (`kd_ruang`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
