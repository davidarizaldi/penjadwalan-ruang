<?php
require('db.php');
date_default_timezone_set('Asia/Kuala_Lumpur');
$results_per_page = 10;

$hari = array(
	'',
	'Senin',
	'Selasa',
	'Rabu',
	'Kamis',
	'Jumat',
	'Sabtu',
	'Minggu'
);
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="refresh" content="<?php $setiap=300; echo $setiap-time()%3600%$setiap; ?>">
  <title>Tabel Ruang Ujian</title>
	
	<link href="dist/css/bootstrap.min.css" rel="stylesheet">
	<style>
		.bd-placeholder-img {
			font-size: 1.125rem;
			text-anchor: middle;
			-webkit-user-select: none;
			-moz-user-select: none;
			-ms-user-select: none;
			user-select: none;
		}

		@media (min-width: 768px) {
			.bd-placeholder-img-lg {
				font-size: 3.5rem;
			}
		}
	</style>
	<link href="nav.css" rel="stylesheet">
</head>

<body>
  <div class="table-responsive h4">
    <table class="table table-hover">
      <thead class="thead-dark">
        <tr class="d-flex">
          <th class="col-2 text-center" scope="col">Tanggal</th>
					<th class="col-2 text-center" scope="col">Waktu</th>
          <th class="col-4 text-center" scope="col">Nama Mahasiswa</th>
          <th class="col-2 text-center" scope="col">Kegiatan</th>
          <th class="col-1 text-center" scope="col">Ruang</th>
					<th class="col-1 text-center" scope="col">Status</th>
        </tr>
      </thead>
      <tbody>
        <?php
        $filter = "WHERE ((tanggal = '".date("Y-m-d")."') AND ('".date("H:i")."' < waktu_akhir)) OR (tanggal > '".date("Y-m-d")."') AND (kegiatan LIKE '%Skripsi%' OR kegiatan LIKE '%Proposal%')";
        $query = "SELECT * FROM jadwal $filter ORDER BY tanggal, waktu_mulai LIMIT 10;";
        $result = $conn->query($query);
        while($row = mysqli_fetch_assoc($result) ) {
        ?>
        <tr class="d-flex<?php echo (date("Y-m-d") == $row["tanggal"] AND date("H:i:s") >= $row["waktu_mulai"])?' table-success':''; ?>" >
          <td class="col-2 text-center"><?php echo $hari[date('N', strtotime($row["tanggal"]))].", ".date('d-m-Y', strtotime($row["tanggal"])); ?></td>
					<td class="col-2 text-center"><?php echo substr($row["waktu_mulai"], 0, -3)." - ".substr($row["waktu_akhir"], 0, -3); ?></td>
          <td class="col-4 text-center"><?php echo $row["nama_mahasiswa"]." (".$row["nim_mahasiswa"].")"; ?></td>
          <td class="col-2 text-center"><?php echo $row["kegiatan"]; ?></td>
          <td class="col-1 text-center"><?php echo $row["kd_ruang"]; ?></td>
					<td class="col-1 text-center"><?php echo (date("Y-m-d") == $row["tanggal"] AND date("H:i:s") >= $row["waktu_mulai"])?'Sedang':''; ?></td>
        </tr>
        <?php } ?>
      </tbody>
    </table>
  </div>
	
	<script src="dist/js/feather.min.js"></script>
	<script src="nav.js"></script>
	<script src="dist/js/jquery.min.js"></script>
	<script src="dist/js/bootstrap.min.js"></script>
	
</body>
</html>
