<?php
require('db.php');
$status = "";
$txtcolor = "text-danger ";
if($_REQUEST['jenis'] == "delete") {
  $query = "DELETE FROM jadwal WHERE id_jadwal= ".$_REQUEST['id_jadwal'].";";
  $result = mysqli_query($conn,$query) or die ( mysqli_error());
  header("Location: manage_jadwal_ujian.php");
} else if($_REQUEST['jenis'] == "insert" AND count($_POST) > 0) {
  $ins_query = "INSERT INTO jadwal(tanggal,waktu_mulai,waktu_akhir,kegiatan,peminjam,nim_mahasiswa,nama_mahasiswa,judul_skripsi,kd_dosen_pembimbing,kd_dosen_penguji,kd_ruang)
                VALUES(
                  '".$_POST['tanggal']."',
                  '".$_POST['waktu_mulai']."',
                  '".$_POST['waktu_akhir']."',
                  '".$_POST['kegiatan']."',
                  '".$_POST['nama_mahasiswa']."',
                  '".$_POST['nim_mahasiswa']."',
                  '".$_POST['nama_mahasiswa']."',
                  '".$_POST['judul_skripsi']."',
                  '".$_POST['kd_dosen_pembimbing']."',
                  '".$_POST['kd_dosen_penguji']."',
                  '".$_POST['kd_ruang']."'
                )";
  $check_query = "SELECT * from jadwal WHERE (tanggal = '".$_POST['tanggal']."') AND ('".$_POST['waktu_mulai']."' < waktu_akhir) AND ('".$_POST['waktu_akhir']."' > waktu_mulai) AND (kd_ruang = '".$_POST['kd_ruang']."');";
  $result = $conn->query($check_query);
  $row = mysqli_fetch_assoc($result);
  if(!empty($row) && $_REQUEST['id_jadwal']!=$row['id_jadwal']) {
    $status = "Jadwal gagal dimasukkan. Ruang ".$row['kd_ruang']." telah digunakan dari ".$row['waktu_mulai']." sampai ".$row['waktu_akhir'];
  } else {
    $check_query = "SELECT * from jadwal WHERE (tanggal = '".$_POST['tanggal']."') AND ('".$_POST['waktu_mulai']."' < waktu_akhir) AND ('".$_POST['waktu_akhir']."' > waktu_mulai) AND (FALSE";
    foreach(explode(",", $_POST['kd_dosen_penguji']) as $kd_dosen) {
      $check_query = $check_query." OR (kd_dosen_penguji LIKE '%".$kd_dosen."%')";
    }
    $check_query = $check_query.");";
    $result = $conn->query($check_query);
    $row = mysqli_fetch_assoc($result);
    if(!empty($row) && $_REQUEST['id_jadwal']!=$row['id_jadwal']) {
      $status = "Jadwal gagal dimasukkan. Tabrakan waktu dosen penguji dari ".$row['waktu_mulai']." sampai ".$row['waktu_akhir'];
    } else {
      $result = $conn->query($ins_query);
      if($result) {
				$status = "Jadwal berhasil dimasukkan.";
				$txtcolor = "text-success ";
			}
      else $status = "Jadwal gagal dimasukkan.";
    }
  }
} else if($_REQUEST['jenis'] == "edit") {
  if(count($_POST) > 0) {
    $edit_query = "UPDATE jadwal
                  SET
                    tanggal = '".$_POST['tanggal']."',
                    waktu_mulai = '".$_POST['waktu_mulai']."',
                    waktu_akhir = '".$_POST['waktu_akhir']."',
                    kegiatan = '".$_POST['kegiatan']."',
                    peminjam = '".$_POST['nama_mahasiswa']."',
                    nim_mahasiswa = '".$_POST['nim_mahasiswa']."',
                    nama_mahasiswa = '".$_POST['nama_mahasiswa']."',
                    judul_skripsi = '".$_POST['judul_skripsi']."',
                    kd_dosen_pembimbing = '".$_POST['kd_dosen_pembimbing']."',
                    kd_dosen_penguji = '".$_POST['kd_dosen_penguji']."',
                    kd_ruang = '".$_POST['kd_ruang']."'
                  WHERE id_jadwal = '".$_REQUEST['id_jadwal']."'";
    $check_query = "SELECT * FROM jadwal WHERE (tanggal = '".$_POST['tanggal']."') AND ('".$_POST['waktu_mulai']."' < waktu_akhir) AND ('".$_POST['waktu_akhir']."' > waktu_mulai) AND (kd_ruang = '".$_POST['kd_ruang']."');";
    $result = $conn->query($check_query);
    $row = mysqli_fetch_assoc($result);
    if(!empty($row) && $_REQUEST['id_jadwal']!=$row['id_jadwal']) {
      $status = "Jadwal gagal diedit. Ruang ".$row['kd_ruang']." telah digunakan dari ".$row['waktu_mulai']." sampai ".$row['waktu_akhir'];
    } else {
      $check_query = "SELECT * FROM jadwal WHERE (tanggal = '".$_POST['tanggal']."') AND ('".$_POST['waktu_mulai']."' < waktu_akhir) AND ('".$_POST['waktu_akhir']."' > waktu_mulai) AND (FALSE";
      foreach(explode(",", $_POST['kd_dosen_penguji']) as $kd_dosen) {
        $check_query = $check_query." OR (kd_dosen_penguji LIKE '%".$kd_dosen."%')";
      }
      $check_query = $check_query.");";
      $result = $conn->query($check_query);
      $row = mysqli_fetch_assoc($result);
      if(!empty($row) && $_REQUEST['id_jadwal']!=$row['id_jadwal']) {
        $status = "Jadwal gagal diedit. Tabrakan waktu dosen penguji dari ".$row['waktu_mulai']." sampai ".$row['waktu_akhir'];
      } else {
        $result = $conn->query($edit_query);
        if($result) {
					$status = "Jadwal berhasil diedit.";
					$txtcolor = "text-success ";
				}
        else $status = "Jadwal gagal diedit.";
      }
    }
  }
  $query = "SELECT * FROM jadwal WHERE id_jadwal='".$_REQUEST['id_jadwal']."';";
  $result = mysqli_query($conn, $query) or die ( mysqli_error());
  $row = mysqli_fetch_assoc($result);
}
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="David Arizaldi Muhammad">
    <title><?php echo ucwords($_REQUEST['jenis']); ?> Jadwal</title>

    <link href="dist/css/bootstrap.min.css" rel="stylesheet">
    <style>
      .bd-placeholder-img {
        font-size: 1.125rem;
        text-anchor: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
      }

      @media (min-width: 768px) {
        .bd-placeholder-img-lg {
          font-size: 3.5rem;
        }
      }
    </style>
    <link href="nav.css" rel="stylesheet">
  </head>
  <body>
    
<nav class="navbar navbar-dark sticky-top bg-dark flex-md-nowrap p-0 shadow">
  <a class="navbar-brand col-md-3 col-lg-2 mr-0 px-3" href="#">FKIP UNRAM</a>
  <button class="navbar-toggler position-absolute d-md-none collapsed" type="button" data-toggle="collapse" data-target="#sidebarMenu" aria-controls="sidebarMenu" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <label class="form-control form-control-dark w-100" type="text"><?php echo strtoupper($_REQUEST['jenis']); ?> JADWAL UJIAN</label>
</nav>

<div class="container-fluid">
  <div class="row">
    <nav id="sidebarMenu" class="col-md-3 col-lg-2 d-md-block bg-light sidebar collapse">
      <div class="sidebar-sticky pt-3">
        <ul class="nav flex-column">
          <li class="nav-item">
            <a class="nav-link" href="index.php">
              <span data-feather="home"></span>
              Ruang Ujian
            </a>
          </li>
        </ul>

        <h6 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted">
          <span>ADMIN</span>
        </h6>
        <ul class="nav flex-column mb-2">
          <li class="nav-item">
            <a class="nav-link" href="manage_jadwal.php">
              <span data-feather="calendar"></span>
              Manage Jadwal
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link active" href="manage_jadwal_ujian.php">
              <span data-feather="calendar"></span>
              Manage Jadwal Ujian <span class="sr-only">(current)</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="manage_ruang.php">
              <span data-feather="box"></span>
              Manage Ruang
            </a>
          </li>
					<li class="nav-item">
            <a class="nav-link" href="export.php">
              <span data-feather="printer"></span>
              Export
            </a>
          </li>
        </ul>
      </div>
    </nav>

    <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-md-4">
      <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-1 pb-2 mb-3">
      </div>
      <div>
        <form name="manage_jadwal_form" id="manage_jadwal_form" method="post" action="">
          <input type="hidden" name="jenis" id="jenis" value="<?php echo $_REQUEST['jenis']; ?>">
          <input type="hidden" name="id_jadwal" id="id_jadwal" value="<?php echo isset($_REQUEST['id_jadwal'])?$_REQUEST['id_jadwal']:''; ?>">
          <div class="form-row">
            <div class="form-group col-md-4">
              <label for="hari">Tanggal</label>
              <input class="form-control" name="tanggal" id="tanggal" type="date" required value="<?php echo isset($row['tanggal'])?$row['tanggal']:date("Y-m-d"); ?>">
            </div>
            <div class="form-group col-md-4">
              <label for="waktu_mulai">Waktu Mulai</label>
              <input class="form-control" type="time" name="waktu_mulai" id="waktu_mulai" required value="<?php echo isset($row['waktu_mulai'])?$row['waktu_mulai']:'08:00'; ?>">
            </div>
            <div class="form-group col-md-4">
              <label for="waktu_akhir">Waktu Akhir</label>
              <input class="form-control" type="time" name="waktu_akhir" id="waktu_akhir" required value="<?php echo isset($row['waktu_akhir'])?$row['waktu_akhir']:'10:00'; ?>">
            </div>
          </div>
          <div class="form-row">
            <div class="form-group col-md-4">
              <label for="nim_mahasiswa">NIM Mahasiswa</label>
              <input type="text" class="form-control border" name="nim_mahasiswa" id="nim_mahasiswa" required value="<?php echo isset($row['nim_mahasiswa'])?$row['nim_mahasiswa']:''; ?>">
            </div>
            <div class="form-group col-md-8">
              <label for="nim_mahasiswa">Nama Mahasiswa</label>
              <input type="text" class="form-control border" name="nama_mahasiswa" id="nama_mahasiswa" required value="<?php echo isset($row['nama_mahasiswa'])?$row['nama_mahasiswa']:''; ?>">
            </div>
          </div>
          <div class="form-row">
            <div class="form-group col-md-4">
              <label for="kegiatan">Kegiatan</label>
              <select name="kegiatan" id="kegiatan" class="custom-select border">
                <option value="" selected required>Kegiatan...</option>
                <option value="Proposal"<?php echo (isset($row["kegiatan"])&&$row["kegiatan"]=='Proposal')?' selected':''; ?>>Proposal</option>
                <option value="Skripsi"<?php echo (isset($row["kegiatan"])&&$row["kegiatan"]=='Skripsi')?' selected':''; ?>>Skripsi</option>
              </select>
            </div>
            <div class="form-group col-md-8">
              <label for="judul_skripsi">Judul Skripsi</label>
              <input type="text" class="form-control border" name="judul_skripsi" id="judul_skripsi" required value="<?php echo isset($row['judul_skripsi'])?$row['judul_skripsi']:''; ?>">
            </div>
          </div>
          <div class="form-row">
            <div class="form-group col-md-4">
              <label for="kd_dosen_pembimbing">Kode Dosen Pembimbing</label>
              <input type="text" class="form-control border" name="kd_dosen_pembimbing" id="kd_dosen_pembimbing" required value="<?php echo isset($row['kd_dosen_pembimbing'])?$row['kd_dosen_pembimbing']:''; ?>">
            </div>
            <div class="form-group col-md-4">
              <label for="kd_dosen_penguji">Kode Dosen Penguji</label>
              <input type="text" class="form-control border" name="kd_dosen_penguji" id="kd_dosen_penguji" required value="<?php echo isset($row['kd_dosen_penguji'])?$row['kd_dosen_penguji']:''; ?>">
            </div>
            <div class="form-group col-md-4">
              <label for="judul_skripsi">Ruang</label>
              <select name="kd_ruang" id="kd_ruang" class="custom-select border">
                <option value="" selected required>Ruang...</option>
                <?php
                $query = "SELECT kd_ruang FROM ruang ORDER BY kd_ruang;";
                $result = $conn->query($query);
                if(mysqli_num_rows($result)!=0) {
                  while($row2 = mysqli_fetch_assoc($result)) {
                    echo "<option value='".$row2["kd_ruang"]."'";
                    echo (isset($row["kd_ruang"])&&$row["kd_ruang"]==$row2["kd_ruang"])?' selected':'';
                    echo ">".$row2["kd_ruang"]."</option>";
                  }
                }
                ?>
              </select>
            </div>
          </div>
          <p class="<?php echo $txtcolor; ?>text-center"><?php if($status!="") echo $status; ?></p>
          <button type="submit" class="btn btn-primary">Submit</button>
        </form>
      </div>
    </main>
  </div>
</div>
    
    <script src="dist/js/feather.min.js"></script>
    <script src="nav.js"></script>
    <script src="dist/js/jquery.min.js"></script>
    <script src="dist/js/bootstrap.min.js"></script>
  </body>
</html>
