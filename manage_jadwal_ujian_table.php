<?php
require('db.php');
date_default_timezone_set('Asia/Kuala_Lumpur');
$results_per_page = 4;
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title>Tabel Manage Jadwal</title>
</head>

<body>
  <div>
    <form method="get">
      <div class="row">
        <div class="col-3">
          <div class="form-group">
            <label>Tanggal Dari</label>
            <a onclick="document.getElementById('tanggal_dari').value = ''"><span data-feather="x-circle"></a>
            <input class="form-control" name="tanggal_dari" id="tanggal_dari" type="date" value="<?php echo isset($_GET['tanggal_dari'])?$_GET['tanggal_dari']:'' ?>">
          </div>
        </div>
        <div class="col-3">
          <div class="form-group">
            <label>Tanggal Sampai</label>
            <a onclick="document.getElementById('tanggal_sampai').value = ''"><span data-feather="x-circle"></a>
            <input class="form-control" name="tanggal_sampai" id="tanggal_sampai" type="date" value="<?php echo isset($_GET['tanggal_sampai'])?$_GET['tanggal_sampai']:'' ?>">
          </div>
        </div>
        <div class="col-3">
          <div class="form-group">
            <label for="waktu_mulai">Waktu Dari</label>
            <a onclick="document.getElementById('waktu_dari').value = ''"><span data-feather="x-circle"></a>
            <input class="form-control" type="time" name="waktu_dari" id="waktu_dari" value="<?php echo isset($_GET['waktu_dari'])?$_GET['waktu_dari']:''; ?>">
          </div>
        </div>
        <div class="col-3">
          <div class="form-group">
            <label for="waktu_sampai">Waktu Sampai</label>
            <a onclick="document.getElementById('waktu_sampai').value = ''"><span data-feather="x-circle"></a>
            <input class="form-control" type="time" name="waktu_sampai" id="waktu_sampai" value="<?php echo isset($_GET['waktu_sampai'])?$_GET['waktu_sampai']:''; ?>">
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-4">
          <div class="form-group">
            <label>Nama Mahasiswa</label>
            <input type="text" name="nama_mahasiswa" id="nama_mahasiswa" class="form-control border" value="<?php echo isset($_GET['nama_mahasiswa'])?$_GET['nama_mahasiswa']:'' ?>" placeholder="David">
          </div>
        </div>
        <div class="col-4">
          <div class="form-group">
            <label>Dosen Pembimbing</label>
            <input type="text" name="kd_dosen_pembimbing" id="kd_dosen_pembimbing" class="form-control border" value="<?php echo isset($_GET['kd_dosen_pembimbing'])?$_GET['kd_dosen_pembimbing']:'' ?>" placeholder="MUN">
          </div>
        </div>
        <div class="col-4">
          <div class="form-group">
            <label>Dosen Penguji</label>
            <input type="text" name="kd_dosen_penguji" id="kd_dosen_penguji" class="form-control border" value="<?php echo isset($_GET['kd_dosen_penguji'])?$_GET['kd_dosen_penguji']:'' ?>" placeholder="UMI">
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-3">
          <div class="form-group">
            <label>Kegiatan</label>
            <select name="kegiatan" id="kegiatan" class="custom-select border">
              <option value="" selected required>Kegiatan...</option>
              <option value="Proposal"<?php echo (isset($_GET["kegiatan"])&&$_GET["kegiatan"]=='Proposal')?' selected':''; ?>>Proposal</option>
              <option value="Skripsi"<?php echo (isset($_GET["kegiatan"])&&$_GET["kegiatan"]=='Skripsi')?' selected':''; ?>>Skripsi</option>
            </select>
          </div>
        </div>
        <div class="col-3">
          <div class="form-group">
            <label>Judul Skripsi</label>
            <input type="text" name="judul_skripsi" id="judul_skripsi" class="form-control border" value="<?php echo isset($_GET['judul_skripsi'])?$_GET['judul_skripsi']:'' ?>" placeholder="Aplikasi Penjadwalan">
          </div>
        </div>
        <div class="col-3">
          <div class="form-group">
            <label>Ruang</label>
            <select name="kd_ruang" id="kd_ruang" class="custom-select border">
              <option value="" selected>Ruang...</option>
              <?php
              $query = "SELECT kd_ruang FROM ruang ORDER BY kd_ruang;";
              $result = $conn->query($query);
              if(mysqli_num_rows($result)!=0) {
                while($row = mysqli_fetch_assoc($result)) {
                  echo "<option value='".$row["kd_ruang"]."'";
                  echo (isset($_GET['kd_ruang'])&&$_GET['kd_ruang']==$row["kd_ruang"])?' selected':'';
                  echo ">".$row["kd_ruang"]."</option>";
                }
              }
              ?>
            </select>
          </div>
        </div>
        <div class="col-3">
          <div class="form-group">
            <label></label>
            <div class="row justify-content-between m-1 mt-2">
              <button type="submit" name="submit" id="submit" value="search" class="btn btn-primary col-3"><span data-feather="search"></span></button>
              <a href="<?php echo $_SERVER['PHP_SELF']; ?>" class="btn btn-danger col-3"><span data-feather="x"></span></a>
              <a type="button" class="btn btn-success col-4"  href="crud_jadwal_ujian.php?jenis=insert">Insert</a>
            </div>
          </div>
        </div>
      </div>
    </form>
  </div>
  <div class="table-responsive">
    <table class="table table-striped table-hover">
      <thead>
        <tr>
          <th scope="col">Tanggal</th>
          <th scope="col">Waktu</th>
          <th scope="col">Kegiatan</th>
          <th scope="col">Nama Mahasiswa</th>
          <th scope="col">Judul Skripsi</th>
          <th scope="col">Pembimbing</th>
          <th scope="col">Penguji</th>
          <th scope="col">Ruang</th>
          <th scope="col">Actions</th>
        </tr>
      </thead>
      <tbody>
        <?php
        $filter = " WHERE TRUE ";
        
        if(isset($_GET['tanggal_dari']) && $_GET['tanggal_dari'] != '')
          $filter = $filter."AND (tanggal >= '".$_GET["tanggal_dari"]."') ";
        if(isset($_GET['tanggal_sampai']) && $_GET['tanggal_sampai'] != '')
          $filter = $filter."AND (tanggal <= '".$_GET["tanggal_sampai"]."') ";
        if(isset($_GET['waktu_dari']) && $_GET['waktu_dari'] != '')
          $filter = $filter."AND (waktu_akhir >= '".$_GET["waktu_dari"]."') ";
        if(isset($_GET['waktu_sampai']) && $_GET['waktu_sampai'] != '')
          $filter = $filter."AND (waktu_mulai <= '".$_GET["waktu_sampai"]."') ";
        if(isset($_GET['nama_mahasiswa']) && $_GET['nama_mahasiswa'] != '')
          $filter = $filter."AND (nama_mahasiswa LIKE '%".$_GET["nama_mahasiswa"]."%') ";
        if(isset($_GET['kd_dosen_pembimbing']) && $_GET['kd_dosen_pembimbing'] != '')
          $filter = $filter."AND (kd_dosen_pembimbing LIKE '%".$_GET["kd_dosen_pembimbing"]."%') ";
        if(isset($_GET['kd_dosen_penguji']) && $_GET['kd_dosen_penguji'] != '')
          $filter = $filter."AND (kd_dosen_penguji LIKE '%".$_GET["kd_dosen_penguji"]."%') ";
        if(isset($_GET['kegiatan']) && $_GET['kegiatan'] != '')
          $filter = $filter."AND (kegiatan LIKE '%".$_GET["kegiatan"]."%') ";
        if(isset($_GET['judul_skripsi']) && $_GET['judul_skripsi'] != '')
          $filter = $filter."AND (judul_skripsi LIKE '%".$_GET["judul_skripsi"]."%') ";
        if(isset($_GET['kd_ruang']) && $_GET['kd_ruang'] != '')
          $filter = $filter."AND (kd_ruang = '".$_GET["kd_ruang"]."') ";
        
        $filter = $filter."AND (kegiatan LIKE '%Skripsi%' OR kegiatan LIKE '%Proposal%') ";
        
        if (isset($_GET["page"])) $page = $_GET["page"];
        else $page = 1;
        $start_from = ($page-1) * $results_per_page;
        
        $query = "SELECT * FROM jadwal $filter ORDER BY tanggal, waktu_mulai LIMIT $start_from, $results_per_page;";
        $result = $conn->query($query);
        while($row = mysqli_fetch_assoc($result) ) {
        ?>
        <tr>
          <td><?php echo $row["tanggal"]; ?></td>
          <td><?php echo substr($row["waktu_mulai"], 0, -3)." - ".substr($row["waktu_akhir"], 0, -3); ?></td>
          <td><?php echo $row["kegiatan"]; ?></td>
          <td><?php echo $row["nama_mahasiswa"]; ?></td>
          <td><?php echo $row["judul_skripsi"]; ?></td>
          <td><?php echo $row["kd_dosen_pembimbing"]; ?></td>
          <td><?php echo $row["kd_dosen_penguji"]; ?></td>
          <td><?php echo $row["kd_ruang"]; ?></td>
          <td>
            <ul class="list-inline m-0">
              <li class="list-inline-item">
                <a href="crud_jadwal_ujian.php?jenis=edit&id_jadwal=<?php echo $row["id_jadwal"]; ?>" class="btn btn-outline-primary btn-sm"><span data-feather="edit"></a>
              </li>
              <li class="list-inline-item">
                <a href="crud_jadwal_ujian.php?jenis=delete&id_jadwal=<?php echo $row["id_jadwal"]; ?>" class="btn btn-outline-danger btn-sm"><span data-feather="trash-2"></a>
              </li>
            </ul>
          </td>
        </tr>
        <?php } ?>
      </tbody>
    </table>
  </div>
  <nav aria-label="Page Navigation">
    <ul class="pagination justify-content-center">
      <?php
      $sql = "SELECT COUNT(*) AS total FROM jadwal ".$filter.";"; 
      $result = $conn->query($sql);
      $row = $result->fetch_assoc();
      $total_pages = ceil($row["total"] / $results_per_page);
      if($total_pages == 0) $total_pages = 1;
      $url=$_SERVER["QUERY_STRING"];
      if(substr($url, 0, 4)=="page") {
        $url = substr($url, strpos($url, "&")+1);
      }
      ?>
      <li class="page-item<?php echo ($page == 1?' disabled':''); ?>">
        <a class="page-link" href="<?php echo $_SERVER["PHP_SELF"]."?page=".($page-1)."&".$url; ?>" aria-label="Previous">
          <span aria-hidden="true">&laquo;</span>
          <span class="sr-only">Previous</span>
        </a>
      </li>
      <?php
      for ($i=1; $i<=$total_pages; $i++) {
        echo "
          <li class='page-item".($page == $i?' active':'')."'>
            <a class='page-link' href='".$_SERVER["PHP_SELF"]."?page=".$i."&".$url."'>$i</a>
          </li>
        ";
      }
      ?>
      <li class="page-item<?php echo ($page == $total_pages?' disabled':''); ?>">
        <a class="page-link" href="<?php echo $_SERVER["PHP_SELF"]."?page=".($page+1)."&".$url; ?>" aria-label="Next">
          <span aria-hidden="true">&raquo;</span>
          <span class="sr-only">Next</span>
        </a>
      </li>
    </ul>
  </nav>
</body>
</html>
