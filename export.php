<?php
require('db.php');
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="David Arizaldi Muhammad">
    <title>Export</title>

    <link href="dist/css/bootstrap.min.css" rel="stylesheet">
    <style>
      .bd-placeholder-img {
        font-size: 1.125rem;
        text-anchor: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
      }

      @media (min-width: 768px) {
        .bd-placeholder-img-lg {
          font-size: 3.5rem;
        }
      }
			.table-wrapper {
				position: relative;
				height: 500px;
				overflow: auto;
			}
    </style>
    <link href="nav.css" rel="stylesheet">
  </head>
  <body>
    
<nav class="navbar navbar-dark sticky-top bg-dark flex-md-nowrap p-0 shadow">
  <a class="navbar-brand col-md-3 col-lg-2 mr-0 px-3" href="#">FKIP UNRAM</a>
  <button class="navbar-toggler position-absolute d-md-none collapsed" type="button" data-toggle="collapse" data-target="#sidebarMenu" aria-controls="sidebarMenu" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <label class="form-control form-control-dark w-100" type="text">EXPORT</label>
</nav>

<div class="container-fluid">
  <div class="row">
    <nav id="sidebarMenu" class="col-md-3 col-lg-2 d-md-block bg-light sidebar collapse">
      <div class="sidebar-sticky pt-3">
        <ul class="nav flex-column">
          <li class="nav-item">
            <a class="nav-link" href="index.php">
              <span data-feather="home"></span>
              Ruang Ujian
            </a>
          </li>
        </ul>

        <h6 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted">
          <span>ADMIN</span>
        </h6>
        <ul class="nav flex-column mb-2">
          <li class="nav-item">
            <a class="nav-link" href="manage_jadwal.php">
              <span data-feather="calendar"></span>
              Manage Jadwal
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="manage_jadwal_ujian.php">
              <span data-feather="calendar"></span>
              Manage Jadwal Ujian
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="manage_ruang.php">
              <span data-feather="box"></span>
              Manage Ruang
            </a>
          </li>
					<li class="nav-item">
            <a class="nav-link active" href="#">
              <span data-feather="printer"></span>
              Export <span class="sr-only">(current)</span>
            </a>
          </li>
        </ul>
      </div>
    </nav>

    <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-md-4">
      <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-1 pb-2 mb-3">
      </div>
      <div class="row">
        <div class="col-6">
					<?php
					$query = "SELECT COUNT(tanggal) AS jumlah FROM jadwal;";
					$result = $conn->query($query);
					$row = mysqli_fetch_assoc($result)
					?>
          <label class="form-control" type="text">Jumlah data jadwal: <?php echo $row["jumlah"]; ?></label>
					<div class="table-responsive table-wrapper">
						<table class="table table-striped table-hover">
							<thead>
								<tr>
									<th scope="col">#</th>
									<th scope="col">Tanggal</th>
									<th scope="col">Waktu</th>
									<th scope="col">Kegiatan</th>
									<th scope="col">Ruang</th>
								</tr>
							</thead>
							<tbody>
								<?php
								$query = "SELECT * FROM jadwal ORDER BY tanggal, waktu_mulai;";
								$result = $conn->query($query);
								$num = 1;
								while($row = mysqli_fetch_assoc($result) ) {
								?>
								<tr>
									<td><?php echo $num; ?></td>
									<td><?php echo $row["tanggal"]; ?></td>
									<td><?php echo substr($row["waktu_mulai"], 0, -3)." - ".substr($row["waktu_akhir"], 0, -3); ?></td>
									<td><?php echo $row["kegiatan"]; ?></td>
									<td><?php echo $row["kd_ruang"]; ?></td>
								</tr>
								<?php $num++; } ?>
							</tbody>
						</table>
					</div>
					<form action="export_jadwal_ruang.php" method="get">
						<div>
							<input type="hidden" name="jenis" id="jenis" value="jadwal">
							<button type="submit" name="export" class="btn btn-success btn-block">Export Jadwal</button>
						</div>
					</form>
        </div>
        <div class="col-6">
          <?php
					$query = "SELECT COUNT(kd_ruang) AS jumlah FROM ruang;";
					$result = $conn->query($query);
					$row = mysqli_fetch_assoc($result)
					?>
          <label class="form-control" type="text">Jumlah data ruang: <?php echo $row["jumlah"]; ?></label>
					<div class="table-responsive table-wrapper">
						<table class="table table-striped table-hover">
							<thead>
								<tr>
									<th scope="col">#</th>
									<th scope="col">Kode Ruang</th>
								</tr>
							</thead>
							<tbody>
								<?php
								$query = "SELECT * FROM ruang ORDER BY kd_ruang;";
								$result = $conn->query($query);
								$num = 1;
								while($row = mysqli_fetch_assoc($result) ) {
								?>
								<tr>
									<td><?php echo $num; ?></td>
									<td><?php echo $row["kd_ruang"]; ?></td>
								</tr>
								<?php $num++; } ?>
							</tbody>
						</table>
					</div>
					<form action="export_jadwal_ruang.php" method="get">
						<div>
							<input type="hidden" name="jenis" id="jenis" value="ruang">
							<button type="submit" name="export" class="btn btn-success btn-block">Export Ruang</button>
						</div>
					</form>
        </div>
      </div>
    </main>
  </div>
</div>
    
    <script src="dist/js/feather.min.js"></script>
    <script src="nav.js"></script>
    <script src="dist/js/jquery.min.js"></script>
    <script src="dist/js/bootstrap.min.js"></script>
  </body>
</html>
